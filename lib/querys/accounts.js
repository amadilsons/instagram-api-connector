const request = require('../services/request');

module.exports = function(page_id, access_token) {
    
    var url = page_id;
    var params = {
        access_token,
        fields: 'connected_instagram_account.fields(name),' + 'instagram_business_account.fields(name)'
    };

    return request(url, params)
    .then(result => { if(result) { return result } else {
        throw new Error('accounts() result is undefined');
    } })
    .catch(err => { throw err; });
}