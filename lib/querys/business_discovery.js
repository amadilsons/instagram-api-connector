const request = require('../services/request');

/**
 * Options {
 * 
 *      username,       //Instagram username of business account to get information from.
 *  
 *      limit,          //Amount of media data to return.
 * 
 *      likes,          //Set to return like_count per posted media
 * 
 *      comments,       //Set to return comments_count per posted media
 * 
 *      after,
 * 
 *      before,     
 * 
 * }
 */
module.exports = function(insta_page_id, access_token, options) {
    
    var url = insta_page_id;
    var params = {
        access_token,
        fields: 'business_discovery',
    };

    if(options.username) {
        params.fields = params.fields.concat('.username(' + options.username + ')');
    }
    else {
        throw new Error('Missing [username] @options parameter.');
    }

    params.fields = params.fields.concat('{followers_count,media_count,media');

    if(options.limit) {
        params.fields = params.fields.concat('.limit(' + options.limit + ')');
    }

    if(options.after) {
        params.fields = params.fields.concat('.after(' + options.after + ')');
    }
    else if(options.before) {
        params.fields = params.fields.concat('.before(' + options.before + ')');
    }

    params.fields = params.fields.concat('{timestamp,media_type')

    if(options.likes) {
        params.fields = params.fields.concat(',like_count');   
    }

    if(options.comments) {
        params.fields = params.fields.concat(',comments_count');            
    }
    
    params.fields = params.fields.concat('}}');

    return request(url, params)
    .then(result => { if(result) { return result } else {
        throw new Error('accounts() result is undefined');
    } })
    .catch(err => { throw err; });
}