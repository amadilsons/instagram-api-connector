const config = __graphConfig;
const accounts = require('./lib/querys/accounts');
const businessDiscovery = require('./lib/querys/business_discovery');

var InstagramConnector = function(configuration)
{
	if (InstagramConnector.caller != InstagramConnector.getInstance)
	{
		throw new Error("This object cannot be instanciated");
    }

    if(config.auth.pageToken) {
        this.access_token = config.auth.pageToken;
    }
}

InstagramConnector.instance = null;
InstagramConnector.getInstance = function()
{
	if (this.instance === null)
	{
		this.instance = new InstagramConnector();
	}
	return this.instance;
};

//High-level methods for facebook api interaction
InstagramConnector.prototype = {

    usePageToken: false, 

    getInstagramAccounts: function(page_id, callback) {

        var access_token = this.access_token;

        accounts(page_id, access_token)
        .then(apiResponse => { 

            delete apiResponse.id;
            callback(null, apiResponse) 
        })
        .catch(err => callback(err) );
    },

    businessDiscovery: function(insta_acc_id, options, callback) {

        var accessToken = this.access_token;
        var busDiscOptions = options;
        options.limit = 100;
        var mediaArray = [];
        var amount = 10;
        var noMorePages = false;
        var result = {};

        if(!options || !options.username) {
            throw new Error('Missing [username] @options parameter.');
        } 
        else {
            busDiscOptions.username = options.username;
        }

        if(options.amount) {
            amount = options.amount;
        }


        var recursive = function(apiResponse) {

            if(apiResponse) {

                mediaArray = mediaArray.concat(apiResponse.business_discovery.media.data);

                if(apiResponse.business_discovery.media.paging.cursors.after) {
                    console.log('yo')
                    busDiscOptions.after = apiResponse.business_discovery.media.paging.cursors.after;
                }
                else {
                    noMorePages = true;
                }
            }
            
            if(noMorePages) {

                mediaArray = mediaArray.map(media => {
                    if(media.media_type) { media.type = media.media_type; delete media.media_type }
                    if(media.like_count) { media.likes = media.like_count; delete media.like_count }
                    if(media.comments_count) { media.comments = media.comments_count; delete media.comments_count }

                    return  media;
                });

                delete result.media;
                result.media = mediaArray;
                result.total = mediaArray.length;
    
                callback(null, result);
            }
            else if(amount <= 100) { //max limit imposed by GraphAPI

                busDiscOptions.limit = amount;

                businessDiscovery(insta_acc_id, accessToken, busDiscOptions).then(lastResponse => {

                    mediaArray = mediaArray.concat(lastResponse.business_discovery.media.data);
                    result = lastResponse.business_discovery;

                    noMorePages = true;

                    recursive();
                    
                }).catch(err => callback(err) );
            } 
            else {
                amount = amount - 100; //remaining number of posts to return
                businessDiscovery(insta_acc_id, accessToken, busDiscOptions).then(recursive).catch(err => callback(err) );
            }
        }   

        new Promise(result => { recursive() });
    }
}

module.exports = InstagramConnector.getInstance();